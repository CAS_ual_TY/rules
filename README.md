---
layout: default
permalink: /
---


# OpenReality rules


1. General:
    1. No racism or insulting (chat, player names, squad names etc.) *too much*.
    2. Do not use a different language than english within public visibility (team chat etc.).
    3. Do not advertise and do not recruit players for your server.
    4. Do not false report any player and do not abuse the report function.
    5. Do not use or show any type of explicit nazi imagery (chat, player names, squad names etc.).
    6. Do not stream on the server unless it has been authorized by the admins.
    7. Do not impersonate any individual.
    8. Do not debate admin decisions in-game.

2. Communication and Teamplay:
    1. Every player must have a working microphone and a correctly configured Mumble.
    2. Keep every type of communication to a minimum.
    3. Squadleaders are required to use direct squad communication (num. 1-9) instead of all-squad communication when appropriate.
    4. All players are required to obey the in-game hierarchy and follow orders.

3. Gameplay:
    1. Do not be a lonewolf.
    2. Do not use any suicide tactics except when playing as insurgents.
    3. Do not request a kit if your squadleader did not tell you to do so.
    4. Do not disrupt the gameplay in any way (teamkilling, smoking main etc.).
    5. Do not create squads before the pre-round timer reaches 2.00!
    6. Do not use any form of an unfair advantage (ghosting, cheating, glitching etc.).
    7. Do not destroy caches with less than 40 players unless it has been authorized by the admins.

4. Rush Protection:
    1. Do not rush unless one of the rushing exceptions is in place or 10 minutes have passed.
    2. Rushing:
        1. Attacking enemies inside the radius of the their first cappable flag.
        2. Entering the radius of the enemy's first cappable flag.
        3. Stopping the enemy from entering the radius of the enemy's first cappable flag.
        4. Intercepting the enemy between main base and their first cappable flag.
    3. Rushing exceptions:
        1. The flag has an attack marker on it.
        2. There are only 2 neutral flags in the beginning on this map.
        3. Your team has paradrop spawns in the beginning on this map.

5. Base Protection:
    1. Main Base:
        1. The compound, building(s) and carrier within any team's main, permanent spawn location.
        2. Repair stations that can not be destroyed.
        3. Dome(s) of Death (DoD), unless any of the exception is in place:
            1. The 1st flag group in order from said main base is cappable by the enemy.
            2. This map is considered a carrier assault map and the DoD is owned by the attacker.
                - Examples: Charlies Point, Jabal Al Burj, Muttrah City, Pavlovsk Bay etc.
    2. Do not attack or fire into the enemy main base unless it has both a flag and an attack marker on it.
    3. Do not fire out of your own main base unless you are using aa emplacements or it has both a flag and an attack marker on it.

6. Squad Types, Rules and Asset Claiming:
    1. "INF"/"Infantry":
        1. This squad may not be locked with less than 6 players.
        2. This squad requires to have an officer kit.
    2. "Mechanized Infantry"/"MECH INF":
        1. This squad may not be locked with less than 6 players.
        2. This squad requires to have an officer kit unless the squadleader is operating the asset or special circumstances would then make the squad less effective.
        3. This squad claims one armored personnel carrier that requires two crewman kits to be operated and has not actively been claimed by "APC".
            - When:
                - IFVs are available: "APC" claims 1 APC,
                - IFVs are not available: "APC" claims 2 APCs
            - Then each "MECH INF" squad claims 1 APC
            -  Then "APC" gets the remaining APCs
    3. "Recon":
        1. This squad may not be locked with less than 4 players.
        2. This squad may only exist once.
    4. "Mortar":
        1. This squad may not have more than 4 players.
        2. This squad may only exist once.
        3. This squad claims all mortar emplacements.
    5. "APC":
        1. This squad may not be locked with less people than required to properly man all assets.
        2. This squad may only exist once.
        3. This squad claims all infantry fighting vehicles.
        4. In case there are infantry fighting vehicles available, this squad claims one chosen amored personnel carrier over "Mechanized Inf"/"MECH INF".
        5. In case there are no infantry fighting vehicles available, this squad claims two chosen armored personnel carriers over "Mechanized Infantry"/"MECH INF".
        6. This squad claims all armored personnel carriers that require two crewman kits to be operated and have not been claimed by "Mechanized Infantry"/"MECH INF".
            - When:
                - IFVs are available: "APC" claims 1 APC,
                - IFVs are not available: "APC" claims 2 APCs
            - Then each "MECH INF" squad claims 1 APC
            -  Then "APC" gets the remaining APCs
        7. This squad claims all reconnaissance vehicles.
    6. "Tank":
        1. This squad may not be locked with less people than required to properly man all assets.
        2. This squad may only exist once.
        3. This squad claims all tanks.
        4. This squad claims all anti-tank missile mounted vehicles.
        5. This squad claims all anti-air vehicles.
    7. "Transport"/"TRANS":
        1. This squad may be locked with 3 or more players.
        2. This squad may only exist once.
        3. This squad claims all transportation helicopters
        4. This squad claims all transportation planes.
    8. "CAS":
        1. This squad may not be locked with less people than required to properly man all assets.
        2. This squad may only exist once.
        3. This squad claims all jets.
        4. This squad claims all attack planes.
        5. This squad claims all attack helicopters.
        6. This squad claims all anti-air vehicles that are not used by "Tank".
    9. Squads can only claim one group of assets at once.
    10. Squads require to be clearly named (not "C4$" etc.) otherwise they are considered as "Infantry"/"Inf".
    11. Do not steal any assets and do not take them if the squadleader does not tell you to do so.
    12. All available assets must be used immediately on spawn if the situation does not lead to an immediate death of the asset after leaving main base (1 jet vs. 4 jets etc.).
    13. Do not take assets if you don't know how to use them.
    14. If the asset is not claimable or it has not yet been claimed by any squad the player who comes first gets controll over the asset until he abandons it.
    15. All assets have to be used properly (not taking AAV just for roadkilling etc.).

7. Aircombat and AA:
    1. As a jet, do not get close to the enemy main base when you are not dogfighting.
    2. As a jet, chasing an enemy jet into their main is permitted until the enemy jet touches the runway.
    3. Do not lock on to enemy helicopters and it's flares or transportation planes and it's flares in or above the enemy main base.
    4. Do not lock on to enemy jets and it's flares or attack planes and it's flares in or above the enemy main base unless you are in a jet, chasing it.
